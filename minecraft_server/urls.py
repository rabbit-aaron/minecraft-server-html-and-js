from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'minecraft_server.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$','mcsite.views.index'),
    url(r'^login/$','mcsite.views.login'),
    url(r'^logout/$','mcsite.views.logout'),
)
