from subprocess import call,PIPE,Popen,STDOUT
from time import sleep
from threading import Thread,Lock
import os,json
from wsgiref.simple_server import make_server
from cgi import parse_qs
from datetime import datetime,timedelta

class ThreadMain(Thread):
  def __init__(self,args,path):
    Thread.__init__(self)
    self.lines = []
    self.args = args.split()
    self.path = path
    self.lock = Lock()
    self.server_thread = None
    self.proc = None

  def run(self):
    os.chdir(self.path)
    self.proc = Popen(self.args,stdout=PIPE,stdin=PIPE,bufsize=1)
    while self.proc.poll() is None:
      line = self.proc.stdout.readline().strip()
      self.lock.acquire()
      if len(self.lines)>1024:
        self.lines.pop(0)
      self.lines.append(line)
      self.lock.release()
    

  def peek_lines(self):
    self.lock.acquire()
    result = self.lines[:]
    self.lock.release()
    return result

  def send_command(self,command):
    self.lock.acquire()
    if len(self.lines)>1024:
      self.lines.pop(0)
    self.lines.append(command.strip())
    self.proc.stdin.write(command.strip()+'\n')
    self.proc.stdin.flush()
    self.lock.release()

  def terminate(self):
    self.lock.acquire()
    self.proc.terminate()
    self.proc.wait()
    self.lock.release()



main_thread = None
g_lock = Lock()
watch_thread = None

class ThreadAutoKill(Thread):
  
  def __init__(self):
    Thread.__init__(self)
    self.starttime = None
    self.lock = Lock()

  def run(self):
    global main_thread,g_lock
    self.renew()
    while True:
      sleep(5)
      if datetime.now() - self.starttime > timedelta(hours=2) and main_thread is not None and main_thread.is_alive():
        g_lock.acquire()
        main_thread.terminate()
        main_thread.join()
        g_lock.release()

  def renew(self):
    global main_thread
    self.lock.acquire()
    self.starttime = datetime.now()
    if len(main_thread.lines)>1024:
      main_thread.lines.pop(0)
    main_thread.lines.append('Session renewed, new shutdown time: %s' % str(self.starttime + timedelta(hours=2)))
    self.lock.release()

def application(environ, start_response):
  global main_thread,g_lock,watch_thread
  d = parse_qs(environ['QUERY_STRING']) # user's GET request parameters
  command = d.get('command',[''])[0]
  start = d.get('start',[''])[0]
  terminate = d.get('terminate',[''])[0]
  renew = d.get('renew',[''])[0]
  g_lock.acquire()
  if start and (main_thread is None or not main_thread.is_alive()):
    main_thread = ThreadMain('java -Xmx1024M -Xms1024M -jar minecraft_server.jar nogui','/home/rabbit/minecraft_server')
    main_thread.start()
    if not watch_thread:
      watch_thread = ThreadAutoKill()
      watch_thread.start()
    else:
      watch_thread.renew()
  elif command and main_thread and main_thread.is_alive():
    main_thread.send_command(command)
  elif terminate and main_thread and main_thread.is_alive():
    main_thread.terminate()
    main_thread.join()
    print "terminated"
  elif renew and main_thread and main_thread.is_alive and watch_thread:
    watch_thread.renew()
  g_lock.release()
  
  if main_thread is not None:
    response_body = json.dumps(main_thread.peek_lines())
  else:
    response_body = json.dumps([])
  status = '200 OK'
  response_headers = [('Content-Type', 'application/javascript'),('Content-Length', str(len(response_body)))]
  start_response(status, response_headers)
  return [response_body]

def main():
  httpd = make_server(
    '127.0.0.1', # The host name.
    8887, # A port number where to wait for the request.
    application # Our application object name, in this case a function.
  )
  httpd.serve_forever()

if __name__ == "__main__":
    main()