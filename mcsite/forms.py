from django import forms
from mcsite.models import *

def bsInput(inputType,placeholder,extraAttrs):
  attrs = {'class':'form-control','placeholder':placeholder}
  attrs.update(extraAttrs)
  return inputType(attrs=attrs)

def bsTextInput(placeholder,extraAttrs={}):
  return bsInput(forms.TextInput,placeholder,extraAttrs)

def bsPasswordInput(placeholder,extraAttrs={}):
  return bsInput(forms.PasswordInput,placeholder,extraAttrs)

def bsNumberInput(placeholder,extraAttrs={}):
  return bsInput(forms.NumberInput,placeholder,extraAttrs)

class FormLogin(forms.Form):
  username = forms.CharField(label='Username',widget=bsTextInput('Username'))
  password = forms.CharField(label='Password',widget=bsPasswordInput('Password'))