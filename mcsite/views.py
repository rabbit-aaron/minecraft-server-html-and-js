from django.shortcuts import render, redirect, get_object_or_404, Http404
from django.contrib.auth.decorators import login_required
from django.contrib import auth
from django.contrib.auth.models import User
from mcsite.forms import FormLogin
import httplib
from django.http import HttpResponse
import errno
from urllib import quote

# Create your views here.
@login_required
def index(request):
  if request.method != 'POST':
    return redirect('/minecraft_runner/')
  data = request.POST
  conn = httplib.HTTPConnection("127.0.0.1:8887")
  try:
    if data.get('start') == 'true':
      conn.request('GET','/?start=true')
    elif data.get('command'):
      conn.request('GET','/?command=%s' % quote(data.get('command')))
    elif data.get('terminate') == 'true':
      conn.request('GET','/?terminate=true')
    elif data.get('renew') == 'true':
      conn.request('GET','/?renew=true')
    else:
       conn.request('GET','/')
  except:
    raise Http404
  r1 = conn.getresponse()
  response_body = r1.read()
  conn.close()
  return HttpResponse(response_body, content_type="application/json")

def login(request):
  if request.user.is_authenticated():
    return redirect('mcsite.views.index')
  if request.method == 'POST':
    username = request.POST.get('username')
    if '@' in username:
      try:
        username = User.objects.get(email=username).username
      except User.DoesNotExist:
        return render(request,'login.html',{'FormLogin':FormLogin(),'Error':'Incorrect Username or Password'})
    password = request.POST.get('password')
    user = auth.authenticate(username=username,password=password)
    if user:
      if user.is_active:
        auth.login(request,user)
        redirect_url = request.GET.get('next')
        if not redirect_url:
          redirect_url = 'mcsite.views.index'
        return redirect(redirect_url)
    return render(request,'login.html',{'FormLogin':FormLogin(),'Error':'Incorrect Username or Password'})
  return render(request,'login.html',{'FormLogin':FormLogin()})

def logout(request):
  auth.logout(request)
  return redirect('mcsite.views.index')