var app = angular.module('Minecraft',['ngCookies','ngSanitize']);
app.controller('MinecraftController',function($scope,$cookies,$sce,MinecraftFactory){
  
  $scope.toLoginPage = function(){
    alert('Please login as administrator first!');
    window.location.href = '/mcsite/login/?next=/minecraft_runner/';
  };

  $scope.logout = function(){
    window.location.href = '/mcsite/logout/';
  };

  $scope.send_command = function(command){
    if($scope.history.length == 0){
      $scope.history.push(command);
    }
    else if(command != $scope.history[$scope.history.length-1]){
      $scope.history.push(command);
    }
    $scope.history_index = $scope.history.length;
    MinecraftFactory.mincraftRequest({'command':command});
    $scope.command = "";
  };

  $scope.start_server = function(){
    MinecraftFactory.mincraftRequest({'start':'true'}).success(function(data){
      $scope.start_clicked = false;
    });
    
  };
  
  $scope.stop_server = function(){
    MinecraftFactory.mincraftRequest({'terminate':'true'}).success(function(data){
      $scope.stop_clicked = false;
    });
  };
  
  $scope.renew_session = function(){
    MinecraftFactory.mincraftRequest({'renew':'true'}).success(function(data){
      $scope.renew_clicked = false;
    });
  };

  $scope.keydown = function(ev){
    switch (ev.which){
      case 38:
        if($scope.history_index >= 1){
          $scope.command=$scope.history[--$scope.history_index];
        }
        break;
      case 40:
        if($scope.history_index < $scope.history.length){
          if(++$scope.history_index == $scope.history.length){
            $scope.command="";
          }else{
            $scope.command=$scope.history[$scope.history_index];
          }
        }
        break;
      default:
        break;
    }
  };

  function init(){
    if(!$cookies.csrftoken){
      $scope.toLoginPage();
    }
    $scope.lines = [];
    $scope.history = [];
    $scope.history_index = 0;
    setInterval(function(){
      $scope.$apply(function(){
        MinecraftFactory.mincraftRequest({}).success(function(data){
          if(typeof data !== 'object'){
            $scope.toLoginPage();
            return;
          }
          if($scope.lines.length != data.length){
            var re = /(\[\d\d:\d\d:\d\d]) (\[.*?\])(.*)/;
            for (var i = 0; i < data.length; ++i){
              var match_result = data[i].match(re);             
              if(match_result && match_result.length == 4){
                data[i] = $sce.trustAsHtml(
                  '<span style="color:rgb(249,38,114)">' + match_result[1] + '</span>' + 
                  '<span style="color:rgb(230,219,116)">' + match_result[2] + '</span>' + 
                  '<span>' + match_result[3] + '</span>');
              }
            }
            $scope.lines = data;          
          };
        });
      });
    },1000);
    $scope.$watch('lines',function(){
      setTimeout(function(){
        var objDiv = document.getElementById("line-wrapper");
        objDiv.scrollTop = objDiv.scrollHeight;
      },618);
    });
  }
  init();
});

app.factory('MinecraftFactory',function($cookies,$http){
  var factory = {};
  factory.mincraftRequest = function(data){
    return $http({
      method:'POST',
      url:'/mcsite/',
      data:$.param(data),
      headers:{
        'X-CSRFToken':$cookies.csrftoken,
        'Content-Type':'application/x-www-form-urlencoded; charset=utf-8',
      },
    });
  };
  return factory;
});
